﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace PdbCruncher
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void CollectionViewSource_Filter(object sender, FilterEventArgs e)
        {
            var item = e.Item as PdbModel.PdbType;
            if (item != null)
            {
                var match = Regex.Match(item?.TypeName, this.tbxFilter.Text, RegexOptions.IgnoreCase);

                e.Accepted = match.Success;

                //if (CultureInfo.CurrentCulture.CompareInfo.IndexOf(item?.TypeName, this.tbxFilter.Text, CompareOptions.IgnoreCase) >= 0)
                //{
                //    e.Accepted = true;
                //}
                //else
                //{
                //    e.Accepted = false;
                //}
            }
        }

        private void TbxFilter_TextChanged(object sender, TextChangedEventArgs e)
        {
            CollectionViewSource.GetDefaultView(viewTypesList.ItemsSource).Refresh();
        }

        private void PdbDataContext_PdbLoadError(object sender, Model.PdbLoadErrorEventArgs args)
        {
            MessageBox.Show(
                this,
                args.Message + Environment.NewLine + args.Exception.Message,
                "PDB Load Error",
                MessageBoxButton.OK,
                MessageBoxImage.Error
            );
        }
    }
}

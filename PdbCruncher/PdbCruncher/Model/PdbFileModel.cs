﻿//using Dia2Lib;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Input;
using System.Linq;
using Microsoft.Win32;
using System.Diagnostics;
using System.Windows.Threading;

namespace PdbCruncher.Model
{
    //public static class PdbSymbolHelpers
    //{
    //    public static PdbSymbol Create(IDiaSymbol symbol)
    //    {
    //        if ((SymTagEnum)symbol.symTag == SymTagEnum.SymTagBaseClass)
    //        {
    //            return new PdbType(symbol);
    //        }
    //        else if (IsValid(symbol))
    //        {
    //            return new PdbMember(symbol);
    //        }

    //        return null;
    //    }
    //    public static bool IsValid(IDiaSymbol symbol)
    //    {
    //        var symtag = (SymTagEnum)symbol.symTag;

    //        switch (symtag)
    //        {
    //            case SymTagEnum.SymTagFunction:
    //            case SymTagEnum.SymTagFriend:
    //            case SymTagEnum.SymTagEnum:
    //            //case SymTagEnum.SymTagVTable:
    //            case SymTagEnum.SymTagUDT:
    //            //case SymTagEnum.SymTagBaseClass:
    //            case SymTagEnum.SymTagBaseType:
    //            case SymTagEnum.SymTagTypedef:
    //                return false;
    //        }

    //        if (symbol.isStatic != 0)
    //        {
    //            return false;
    //        }

    //        if (symbol.locationType != 4 && symbol.locationType != 0 && symbol.locationType != 6)
    //        {
    //            return false;
    //        }

    //        return true;
    //    }
    //}

    //public class PdbSymbol : BaseModel
    //{
    //    protected IDiaSymbol Symbol { get; set; }

    //    public ulong Size { get; protected set; }
    //    public ulong Padding { get; protected set; }
    //    public long Offset { get; protected set; }

    //    public PdbSymbol(IDiaSymbol symbol)
    //    {
    //        this.Symbol = symbol;
    //    }
    //}

    //public class PdbEnding : PdbSymbol
    //{
    //    public PdbEnding(long offset, ulong size)
    //        : base(null)
    //    {
    //        this.Offset = offset;
    //        this.Size = size;
    //    }
    //}

    //public class PdbPadding : PdbSymbol
    //{
    //    public PdbPadding(long offset, ulong size)
    //        : base(null)
    //    {
    //        this.Offset = offset;
    //        this.Size = size;
    //    }
    //}

    //public class PdbType : PdbSymbol
    //{
    //    public string TypeName { get; private set; }
    //    public ObservableCollection<PdbSymbol> Children { get; private set; }

    //    public PdbType(IDiaSymbol symbol)
    //        : base(symbol)
    //    {
    //        this.TypeName = symbol.name;
    //        this.Offset = (long)symbol.offset;
    //        this.Size = symbol.length;

    //        var symbols = new List<PdbSymbol>();

    //        symbol.findChildrenEx(SymTagEnum.SymTagNull, null, 0U, out IDiaEnumSymbols children);

    //        var nativeSymbols = new List<IDiaSymbol>();

    //        foreach (IDiaSymbol child in children)
    //        {
    //            nativeSymbols.Add(child);
    //        }

    //        nativeSymbols.Sort((x, y) => x.offset.CompareTo(y.offset));

    //        var validSymbols = new List<KeyValuePair<IDiaSymbol, PdbSymbol>>();
    //        foreach (var current in nativeSymbols)
    //        {
    //            var item = PdbSymbolHelpers.Create(current);
    //            if (item != null)
    //            {
    //                validSymbols.Add(new KeyValuePair<IDiaSymbol, PdbSymbol>(current, item));
    //            }
    //        }

    //        if (validSymbols.Count > 1)
    //        {
    //            for (var i = 0; i < validSymbols.Count - 1; ++i)
    //            {
    //                var current = validSymbols[i].Value;
    //                var adjacent = validSymbols[i + 1].Value;

    //                symbols.Add(current);

    //                if (current.Size != 0 && adjacent.Size != 0)
    //                {
    //                    //
    //                    // Symbols differ in length
    //                    //

    //                    if (current.Offset != adjacent.Offset)
    //                    {
    //                        //
    //                        // Symbols aren't in union with each other.
    //                        //

    //                        var current_offset = current.Offset + (long)current.Size;
    //                        var adjacent_offset = adjacent.Offset;

    //                        if (current_offset != adjacent_offset)
    //                        {
    //                            //
    //                            // There is actual difference between offsets.
    //                            //

    //                            var padding = adjacent_offset - current_offset;

    //                            if (padding != 0)
    //                            {
    //                                //
    //                                // There is padding.
    //                                //

    //                                symbols.Add(new PdbPadding(current_offset, (ulong)padding));
    //                            }
    //                        }
    //                    }
    //                }
    //            }

    //            symbols.Add(validSymbols.Last().Value);
    //        }
    //        else if(validSymbols.Count == 1)
    //        {
    //            var first = validSymbols.First();
    //            symbols.Add(first.Value);
    //        }

    //        var last = validSymbols.LastOrDefault();

    //        if (last.Value != null)
    //        {
    //            if (last.Value.Size != 0)
    //            {
    //                var final_offset = last.Value.Offset + (long)last.Value.Size;
    //                var final_padding = (long)symbol.length - final_offset;

    //                if (final_padding != 0)
    //                {
    //                    symbols.Add(new PdbEnding(final_offset, (ulong)final_padding));
    //                }
    //            }
    //        }


    //        this.Children = new ObservableCollection<PdbSymbol>();
    //        this.Children.AddRange(symbols);
    //    }
    //}

    //public class PdbMember : PdbSymbol
    //{
    //    public string Name { get; private set; }
    //    public string TypeName { get; private set; }

    //    public ulong? BitStart { get; private set; }
    //    public ulong? BitLength { get; private set; }

    //    public SymTagEnum Tag { get; private set; }

    //    public static string GetFunctionTypeName(IDiaSymbol symbol)
    //    {
    //        var result = string.Empty;

    //        var resultType = symbol.type;
    //        if (resultType != null)
    //        {
    //            result += GetTypeName(resultType);
    //        }

    //        result += symbol.name;

    //        symbol.findChildrenEx(SymTagEnum.SymTagNull, null, 0, out IDiaEnumSymbols children);

    //        var items = new List<string>();
    //        foreach (IDiaSymbol child in children)
    //        {
    //            items.Add(GetTypeName(child));
    //        }

    //        result += "(";
    //        result += string.Join(", ", items);
    //        result += ")";

    //        return result;
    //    }

    //    public static string GetBound(IDiaSymbol bound)
    //    {
    //        return "<bound>";
    //    }

    //    public static string GetArrayName(IDiaSymbol symbol)
    //    {
    //        var result = string.Empty;

    //        if (symbol != null)
    //        {
    //            var type = symbol.type;
    //            if (type != null)
    //            {
    //                result += GetTypeName(type);

    //                var done = false;

    //                var rank = symbol.rank;
    //                if (rank != 0)
    //                {
    //                    symbol.findChildren(SymTagEnum.SymTagDimension, null, 0, out IDiaEnumSymbols bounds);

    //                    if (bounds != null)
    //                    {
    //                        foreach (IDiaSymbol bound in bounds)
    //                        {
    //                            done = true;

    //                            result += "[";
    //                            result += GetBound(bound.lowerBound);
    //                            result += "..";
    //                            result += GetBound(bound.upperBound);
    //                            result += "]";
    //                        }
    //                    }
    //                }

    //                if (!done)
    //                {
    //                    symbol.findChildren(SymTagEnum.SymTagCustomType, null, 0, out IDiaEnumSymbols boundTypes);

    //                    if (boundTypes != null)
    //                    {

    //                        foreach (IDiaSymbol boundType in boundTypes)
    //                        {
    //                            done = true;
    //                            result += "[";
    //                            result += GetTypeName(boundType);
    //                            result += "]";
    //                        }
    //                    }
    //                }

    //                if (!done)
    //                {
    //                    var count = symbol.count;

    //                    if (count != 0)
    //                    {
    //                        done = true;

    //                        result += "[" + count.ToString() + "]";
    //                    }
    //                }

    //                if (!done)
    //                {
    //                    var lengthArray = symbol.length;
    //                    var lengthElement = type.length;

    //                    done = true;
    //                    if (lengthElement == 0)
    //                    {
    //                        result += "[" + lengthArray.ToString() + "]";
    //                    }
    //                    else
    //                    {
    //                        result += "[" + (lengthArray / lengthElement).ToString() + "]";
    //                    }
    //                }

    //                if (!done)
    //                {
    //                    result = "<array>[]";
    //                }
    //            }
    //        }

    //        return result;
    //    }

    //    public static string GetTypeName(IDiaSymbol symbol)
    //    {
    //        var result = string.Empty;

    //        if (symbol != null)
    //        {
    //            var tag = (SymTagEnum)symbol.symTag;

    //            switch (tag)
    //            {
    //                case SymTagEnum.SymTagFunctionArgType:
    //                    {
    //                        result = GetTypeName(symbol.type);
    //                        break;
    //                    }
    //                case SymTagEnum.SymTagFunctionType:
    //                    {
    //                        result = GetFunctionTypeName(symbol);
    //                        break;
    //                    }
    //                case SymTagEnum.SymTagPointerType:
    //                    {
    //                        result = GetTypeName(symbol.type);

    //                        if (symbol.constType != 0)
    //                        {
    //                            result += "const ";
    //                        }

    //                        if (symbol.volatileType != 0)
    //                        {
    //                            result += "volatile ";
    //                        }

    //                        if (symbol.unalignedType != 0)
    //                        {
    //                            result += "__unaligned ";
    //                        }

    //                        if (symbol.reference != 0)
    //                        {
    //                            result += "& ";
    //                        }
    //                        else
    //                        {
    //                            result += "* ";
    //                        }

    //                        break;
    //                    }
    //                case SymTagEnum.SymTagArrayType:
    //                    {
    //                        result = GetArrayName(symbol);
    //                        break;
    //                    }
    //                case SymTagEnum.SymTagVTable:
    //                case SymTagEnum.SymTagVTableShape:
    //                    {
    //                        result += "<vtable>";
    //                        break;
    //                    }
    //                case SymTagEnum.SymTagUDT:
    //                    {
    //                        var kind = symbol.udtKind;

    //                        switch (kind)
    //                        {
    //                            case 0:
    //                                result += "struct ";
    //                                break;
    //                            case 1:
    //                                result += "class ";
    //                                break;
    //                            case 2:
    //                                result += "union ";
    //                                break;
    //                            case 3:
    //                                result += "interface ";
    //                                break;
    //                        }

    //                        result += symbol.name;
    //                        break;
    //                    }
    //                case SymTagEnum.SymTagEnum:
    //                    {
    //                        result = "enum " + symbol.name;
    //                        break;
    //                    }
    //                case SymTagEnum.SymTagBaseType:
    //                    {
    //                        result = BasicTypeHelpers.GetName(symbol);
    //                        break;
    //                    }
    //            }
    //        }

    //        return result;
    //    }

    //    public PdbMember(IDiaSymbol symbol)
    //        : base(symbol)
    //    {
    //        Size = symbol.type.length;
    //        Name = symbol.name;
    //        Offset = symbol.offset;
    //        Tag = (SymTagEnum)symbol?.type?.symTag;

    //        var locType = (LocationType)symbol.locationType;

    //        switch (locType)
    //        {
    //            case LocationType.LocIsBitField:
    //                {
    //                    this.BitStart = symbol.bitPosition;
    //                    this.BitLength = symbol.length;
    //                    break;
    //                }
    //            default:
    //                {
    //                    break;
    //                }
    //        }

    //        this.TypeName = GetTypeName(symbol.type);
    //    }
    //}

    public class PdbLoadErrorEventArgs : EventArgs
    {
        public PdbLoadErrorEventArgs(string message)
            : this(message, null)
        {
        }
        public PdbLoadErrorEventArgs(string message, Exception exception)
        {
            this.Message = message;
            this.Exception = exception;
        }

        public string Message { get; private set; }
        public Exception Exception { get; private set; }
    }

    public delegate void PdbLoadErrorEventHandler(object sender, PdbLoadErrorEventArgs args);

    public class PdbDataContext : BaseModel
    {
        private PdbModel.PdbFile m_File;

        public IReadOnlyList<PdbModel.PdbType> Types => this.m_File?.Types;

        private PdbModel.PdbTypeDescriptor m_Descriptor;

        public PdbModel.PdbTypeDescriptor Descriptor => this.m_Descriptor;

        public PdbModel.PdbType CurrentType
        {
            set
            {
                if (value != null)
                {
                    this.m_Descriptor = new PdbModel.PdbTypeDescriptor(value);
                    this.RaisePropertyChanged(nameof(this.Descriptor)); 
                }
            }
        }


        private bool m_IsLoading = false;
        public bool IsLoading
        {
            get
            {
                return this.m_IsLoading;
            }
            private set
            {
                this.m_IsLoading = value;
                RaisePropertyChanged();
            }
        }

        public ICommand CommandLoad { get; private set; }

        public event PdbLoadErrorEventHandler PdbLoadError;

        private void RaisePdbLoadError(Exception exception)
        {
            var handler = this.PdbLoadError;
            if (handler != null)
            {
                handler(this, new PdbLoadErrorEventArgs("Error during loading PDB file", exception));
            }
        }

        public PdbDataContext()
        {
            this.CommandLoad = new RelayCommand(this.CommandHandler_Load);
        }

        private void CommandHandler_Load(object obj)
        {
            var dialog = new OpenFileDialog()
            {
                Filter = "Pdb files(*.pdb)|*.pdb|All files (*.*)|*.*"
            };

            if (dialog.ShowDialog() == true)
            {
                this.Load(dialog.FileName);
            }
        }

        public void Load(string path)
        {
            this.m_File = new PdbModel.PdbFile(path);
            this.RaisePropertyChanged(nameof(this.Types));

            //var worker = new BackgroundWorker();

            //worker.DoWork += (object sender, DoWorkEventArgs e) =>
            //{
            //};

            //worker.RunWorkerCompleted += (object sender, RunWorkerCompletedEventArgs e) =>
            //{
            //    if (e.Cancelled)
            //    {
            //        // TODO:
            //    }
            //    else if (e.Error != null)
            //    {
            //        this.RaisePdbLoadError(e.Error);
            //    }

            //    this.IsLoading = false;
            //    this.RaisePropertyChanged(nameof(this.Types));
            //};

            //this.IsLoading = true;
            //worker.RunWorkerAsync();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace PdbCruncher.Model
{
    public static class ObservableCollectionHelpers
    {
        public static void AddRange<T>(this ObservableCollection<T> @this, IEnumerable<T> values)
        {
            foreach (var item in values)
            {
                @this.Add(item);
            }
        }
    }

    public class BaseModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
    public class RelayCommand : ICommand
    {
        private Action<object> m_Action;
        private Func<object, bool> m_CanExecute;

        public event EventHandler CanExecuteChanged;

        public RelayCommand(Action<object> action)
            : this(action, null)
        {
        }

        public RelayCommand(Action<object> action, Func<object, bool> canExecute)
        {
            this.m_Action = action;
            this.m_CanExecute = canExecute;
        }


        public bool CanExecute(object parameter)
        {
            var handler = this.m_CanExecute;
            if (handler != null)
            {
                return handler(parameter);
            }

            return true;
        }

        public void Execute(object parameter)
        {
            this.m_Action(parameter);
        }
    }
}

﻿//using Dia2Lib;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace PdbCruncher.Model
{
    [SuppressUnmanagedCodeSecurity]
    internal static class SafeNativeMethods
    {
        [DllImport("shlwapi.dll", CharSet = CharSet.Unicode)]
        public static extern int StrCmpLogicalW(string psz1, string psz2);
    }

    public sealed class NaturalStringComparer : IComparer<string>
    {
        public int Compare(string a, string b)
        {
            return SafeNativeMethods.StrCmpLogicalW(a, b);
        }
    }

    public sealed class NaturalFileInfoNameComparer : IComparer<FileInfo>
    {
        public int Compare(FileInfo a, FileInfo b)
        {
            return SafeNativeMethods.StrCmpLogicalW(a.Name, b.Name);
        }
    }

    public enum BasicType
    {
        btNoType = 0,
        btVoid = 1,
        btChar = 2,
        btWChar = 3,
        btSChar = 4,
        btUChar = 5,
        btInt = 6,
        btUInt = 7,
        btFloat = 8,
        btBCD = 9,
        btBool = 10,
        btShort = 11,
        btUShort = 12,
        btLong = 13,
        btULong = 14,
        btInt8 = 15,
        btInt16 = 16,
        btInt32 = 17,
        btInt64 = 18,
        btInt128 = 19,
        btUInt8 = 20,
        btUInt16 = 21,
        btUInt32 = 22,
        btUInt64 = 23,
        btUInt128 = 24,
        btCurrency = 25,
        btDate = 26,
        btVariant = 27,
        btComplex = 28,
        btBit = 29,
        btBSTR = 30,
        btHresult = 31,
        btChar16 = 32,
        btChar32 = 33,
        btChar8 = 34,
    };

    public static class BasicTypeHelpers
    {
        //public static string GetName(IDiaSymbol type)
        //{
        //    var baseType = (BasicType)type.baseType;

        //    switch (baseType)
        //    {
        //        case BasicType.btNoType:
        //            return "<no-type>";
        //        case BasicType.btVoid:
        //            return "void";
        //        case BasicType.btChar:
        //            return "char";
        //        case BasicType.btWChar:
        //            return "wchar_t";
        //        case BasicType.btSChar:
        //            return "signed char";
        //        case BasicType.btUChar:
        //            return "unsigned char";
        //        case BasicType.btInt:
        //        case BasicType.btUInt:
        //            {
        //                var result = baseType == BasicType.btUInt ? "unsigned " : string.Empty;

        //                switch (type.length)
        //                {
        //                    case 1:
        //                        result += "char";
        //                        break;
        //                    case 2:
        //                        result += "short";
        //                        break;
        //                    case 4:
        //                        result += "int";
        //                        break;
        //                    case 8:
        //                        result += "long long";
        //                        break;
        //                }

        //                return result;
        //            }
        //        case BasicType.btFloat:
        //            {
        //                if (type.length == 4)
        //                {
        //                    return "float";
        //                }
        //                else if (type.length == 8)
        //                {
        //                    return "double";
        //                }
        //                else if (type.length == 2)
        //                {
        //                    return "half";
        //                }

        //                return "<unknown-float>";
        //            }
        //        case BasicType.btBCD:
        //            return "<bcd>";
        //        case BasicType.btBool:
        //            return "bool";
        //        case BasicType.btShort:
        //            return "short";
        //        case BasicType.btUShort:
        //            return "unsigned short";
        //        case BasicType.btLong:
        //            return "long";
        //        case BasicType.btULong:
        //            return "unsigned long";
        //        case BasicType.btInt8:
        //            return "__int8";
        //        case BasicType.btInt16:
        //            return "__int16";
        //        case BasicType.btInt32:
        //            return "__int32";
        //        case BasicType.btInt64:
        //            return "__int64";
        //        case BasicType.btInt128:
        //            return "__int128";
        //        case BasicType.btUInt8:
        //            return "__uint8";
        //        case BasicType.btUInt16:
        //            return "__uint16";
        //        case BasicType.btUInt32:
        //            return "__uint32";
        //        case BasicType.btUInt64:
        //            return "__uint64";
        //        case BasicType.btUInt128:
        //            return "__uint128";
        //        case BasicType.btCurrency:
        //            return "<currency>";
        //        case BasicType.btDate:
        //            return "<date>";
        //        case BasicType.btVariant:
        //            return "<variant>";
        //        case BasicType.btComplex:
        //            return "<complex>";
        //        case BasicType.btBit:
        //            return "<bit>";
        //        case BasicType.btBSTR:
        //            return "BSTR";
        //        case BasicType.btHresult:
        //            return "HRESULT";
        //        case BasicType.btChar16:
        //            return "char16_t";
        //        case BasicType.btChar32:
        //            return "char32_t";
        //        case BasicType.btChar8:
        //            return "char8_t";
        //    }

        //    throw new ArgumentOutOfRangeException(nameof(type));
        //}
    }



    public enum LocationType
    {
        LocIsNull,
        LocIsStatic,
        LocIsTLS,
        LocIsRegRel,
        LocIsThisRel,
        LocIsEnregistered,
        LocIsBitField,
        LocIsSlot,
        LocIsIlRel,
        LocInMetaData,
        LocIsConstant,
        LocIsRegRelAliasIndir,
        LocTypeMax
    };

    public enum DataKind
    {
        DataIsUnknown,
        DataIsLocal,
        DataIsStaticLocal,
        DataIsParam,
        DataIsObjectPtr,
        DataIsFileStatic,
        DataIsGlobal,
        DataIsMember,
        DataIsStaticMember,
        DataIsConstant
    };

    public enum UdtKind
    {
        UdtStruct,
        UdtClass,
        UdtUnion,
        UdtInterface
    };
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PdbModel
{
    public class PdbException : Exception
    {
        public PdbException(string message)
            : base(message)
        {
        }

        public PdbException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}

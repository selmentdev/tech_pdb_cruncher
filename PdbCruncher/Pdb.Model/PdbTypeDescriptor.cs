﻿using Dia2Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PdbModel
{
    internal static class PdbTypeNameHelpers
    {
        public static string GetFunctionTypeName(IDiaSymbol symbol)
        {
            var result = string.Empty;

            var resultType = symbol.type;
            if (resultType != null)
            {
                result += GetTypeName(resultType);
            }

            result += symbol.name;

            symbol.findChildrenEx(SymTagEnum.SymTagNull, null, 0, out IDiaEnumSymbols children);

            var items = new List<string>();
            foreach (IDiaSymbol child in children)
            {
                items.Add(GetTypeName(child));
            }

            result += "(";
            result += string.Join(", ", items);
            result += ")";

            return result;
        }

        public static string GetBound(IDiaSymbol bound)
        {
            return "<bound>";
        }

        public static string GetArrayName(IDiaSymbol symbol)
        {
            var result = string.Empty;

            if (symbol != null)
            {
                var type = symbol.type;
                if (type != null)
                {
                    result += GetTypeName(type);

                    var done = false;

                    var rank = symbol.rank;
                    if (rank != 0)
                    {
                        symbol.findChildren(SymTagEnum.SymTagDimension, null, 0, out IDiaEnumSymbols bounds);

                        if (bounds != null)
                        {
                            foreach (IDiaSymbol bound in bounds)
                            {
                                done = true;

                                result += "[";
                                result += GetBound(bound.lowerBound);
                                result += "..";
                                result += GetBound(bound.upperBound);
                                result += "]";
                            }
                        }
                    }

                    if (!done)
                    {
                        symbol.findChildren(SymTagEnum.SymTagCustomType, null, 0, out IDiaEnumSymbols boundTypes);

                        if (boundTypes != null)
                        {

                            foreach (IDiaSymbol boundType in boundTypes)
                            {
                                done = true;
                                result += "[";
                                result += GetTypeName(boundType);
                                result += "]";
                            }
                        }
                    }

                    if (!done)
                    {
                        var count = symbol.count;

                        if (count != 0)
                        {
                            done = true;

                            result += "[" + count.ToString() + "]";
                        }
                    }

                    if (!done)
                    {
                        var lengthArray = symbol.length;
                        var lengthElement = type.length;

                        done = true;
                        if (lengthElement == 0)
                        {
                            result += "[" + lengthArray.ToString() + "]";
                        }
                        else
                        {
                            result += "[" + (lengthArray / lengthElement).ToString() + "]";
                        }
                    }

                    if (!done)
                    {
                        result = "<array>[]";
                    }
                }
            }

            return result;
        }

        public static string GetTypeName(IDiaSymbol symbol)
        {
            var result = string.Empty;

            if (symbol != null)
            {
                var tag = (SymTagEnum)symbol.symTag;

                switch (tag)
                {
                    case SymTagEnum.SymTagFunctionArgType:
                        {
                            result = GetTypeName(symbol.type);
                            break;
                        }
                    case SymTagEnum.SymTagFunctionType:
                        {
                            result = GetFunctionTypeName(symbol);
                            break;
                        }
                    case SymTagEnum.SymTagPointerType:
                        {
                            result = GetTypeName(symbol.type);

                            if (symbol.constType != 0)
                            {
                                result += "const ";
                            }

                            if (symbol.volatileType != 0)
                            {
                                result += "volatile ";
                            }

                            if (symbol.unalignedType != 0)
                            {
                                result += "__unaligned ";
                            }

                            if (symbol.reference != 0)
                            {
                                result += "& ";
                            }
                            else
                            {
                                result += "* ";
                            }

                            break;
                        }
                    case SymTagEnum.SymTagArrayType:
                        {
                            result = GetArrayName(symbol);
                            break;
                        }
                    case SymTagEnum.SymTagVTable:
                    case SymTagEnum.SymTagVTableShape:
                        {
                            result += "<vtable>";
                            break;
                        }
                    case SymTagEnum.SymTagUDT:
                        {
                            var kind = symbol.udtKind;

                            switch (kind)
                            {
                                case 0:
                                    result += "struct ";
                                    break;
                                case 1:
                                    result += "class ";
                                    break;
                                case 2:
                                    result += "union ";
                                    break;
                                case 3:
                                    result += "interface ";
                                    break;
                            }

                            result += symbol.name;
                            break;
                        }
                    case SymTagEnum.SymTagEnum:
                        {
                            result = "enum " + symbol.name;
                            break;
                        }
                    case SymTagEnum.SymTagBaseType:
                        {
                            result = BasicTypeHelpers.GetName(symbol);
                            break;
                        }
                }
            }

            return result;
        }
    }

    public enum PdbMemberAccess
    {
        Internal,
        Inherited,
        Public,
        Private,
        Protected,
    }

    public abstract class PdbBaseMember
    {
        public abstract PdbMemberAccess Access { get; }
        public ulong Size { get; protected set; }
        public long Offset { get; protected set; }

        public long NextMemberOffset
        {
            get
            {
                return this.Offset + (long)this.Size;
            }
        }
    }

    public class PdbPadding : PdbBaseMember
    {
        public PdbPadding(long offset, ulong size)
        {
            this.Offset = offset;
            this.Size = size;
        }

        public override PdbMemberAccess Access => PdbMemberAccess.Internal;
    }

    public class PdbEnding : PdbBaseMember
    {
        public PdbEnding(long offset, ulong size)
        {
            this.Offset = offset;
            this.Size = size;
        }

        public override PdbMemberAccess Access => PdbMemberAccess.Internal;
    }

    public class PdbMember : PdbBaseMember
    {
        protected PdbMemberAccess m_Access;

        public override PdbMemberAccess Access => this.m_Access;

        public string Name { get; private set; }
        public string TypeName { get; private set; }
        public ulong? BitStart { get; private set; }
        public ulong? BitLength { get; private set; }


        internal PdbMember(IDiaSymbol symbol)
        {
            this.m_Access = PdbSymbolFactory.From((CV_access_e)symbol.access);
            this.Size = symbol.type.length;
            this.Name = symbol.name;
            this.Offset = symbol.offset;
            this.TypeName = PdbTypeNameHelpers.GetTypeName(symbol.type);

            var location = (LocationType)symbol.locationType;

            switch (location)
            {
                case LocationType.LocIsBitField:
                    {
                        this.BitStart = symbol.bitPosition;
                        this.BitLength = symbol.length;
                        break;
                    }
                default:
                    {
                        break;
                    }
            }
        }
    }

    public class PdbTypeMember : PdbBaseMember
    {
        public override PdbMemberAccess Access => PdbMemberAccess.Inherited;
        public string TypeName { get; private set; }

        public IReadOnlyList<PdbBaseMember> Members => this.m_Members;

        private List<PdbBaseMember> m_Members;

        internal PdbTypeMember(IDiaSymbol symbol)
        {
            this.TypeName = symbol.name;
            this.Offset = (long)symbol.offset;
            this.Size = symbol.length;

            var symbols = new List<PdbBaseMember>();

            symbol.findChildrenEx(SymTagEnum.SymTagNull, null, 0U, out IDiaEnumSymbols children);

            var nativeSymbols = new List<IDiaSymbol>();

            foreach (IDiaSymbol child in children)
            {
                nativeSymbols.Add(child);
            }

            nativeSymbols.Sort((x, y) => x.offset.CompareTo(y.offset));

            var validSymbols = new List<KeyValuePair<IDiaSymbol, PdbBaseMember>>();
            foreach (var current in nativeSymbols)
            {
                var item = PdbSymbolFactory.Create(current);
                if (item != null)
                {
                    validSymbols.Add(new KeyValuePair<IDiaSymbol, PdbBaseMember>(current, item));
                }
            }

            foreach (var current in validSymbols)
            {
                var currentSymbol = current.Value;

                //
                // Find closest symbol to current one.
                //

                var sorted = validSymbols.OrderBy(x => Math.Abs(x.Value.NextMemberOffset - currentSymbol.Offset));
                var previous = sorted.FirstOrDefault();

                if(previous.Value != currentSymbol && previous.Value.NextMemberOffset < currentSymbol.Offset)
                {
                    //
                    // These two symbols introduce padding.
                    //

                    symbols.Add(new PdbPadding(previous.Value.NextMemberOffset, (ulong)(currentSymbol.Offset - previous.Value.NextMemberOffset)));
                }

                symbols.Add(currentSymbol);
            }

            var last = validSymbols.LastOrDefault();

            if (last.Value != null)
            {
                if (last.Value.Size != 0)
                {
                    var final_offset = last.Value.Offset + (long)last.Value.Size;
                    var final_padding = (long)symbol.length - final_offset;

                    if (final_padding != 0)
                    {
                        symbols.Add(new PdbEnding(final_offset, (ulong)final_padding));
                    }
                }
            }

            this.m_Members = symbols;
        }
    }

    internal static class PdbSymbolFactory
    {
        public static PdbMemberAccess From(CV_access_e access)
        {
            switch (access)
            {
                case CV_access_e.CV_private:
                    return PdbMemberAccess.Private;
                case CV_access_e.CV_protected:
                    return PdbMemberAccess.Protected;
                case CV_access_e.CV_public:
                    return PdbMemberAccess.Public;
            }

            return PdbMemberAccess.Internal;
        }

        public static PdbBaseMember Create(IDiaSymbol symbol)
        {
            if ((SymTagEnum)symbol.symTag == SymTagEnum.SymTagBaseClass)
            {
                return new PdbTypeMember(symbol);
            }
            else if (IsValidMember(symbol))
            {
                return new PdbMember(symbol);
            }

            return null;
        }

        public static bool IsValidMember(IDiaSymbol symbol)
        {
            var symtag = (SymTagEnum)symbol.symTag;

            switch (symtag)
            {
                case SymTagEnum.SymTagFunction:
                case SymTagEnum.SymTagFriend:
                case SymTagEnum.SymTagEnum:
                //case SymTagEnum.SymTagVTable:
                case SymTagEnum.SymTagUDT:
                //case SymTagEnum.SymTagBaseClass:
                case SymTagEnum.SymTagBaseType:
                case SymTagEnum.SymTagTypedef:
                    return false;
            }

            if (symbol.isStatic != 0)
            {
                return false;
            }

            if (symbol.locationType != 4 && symbol.locationType != 0 && symbol.locationType != 6)
            {
                return false;
            }

            return true;
        }
    }

    public class PdbTypeDescriptor
    {
        private List<PdbBaseMember> m_Members;
        public IReadOnlyList<PdbBaseMember> Members => this.m_Members;

        public PdbTypeDescriptor(PdbType symbol)
        {
            if (symbol.Symbol != null)
            {
                this.m_Members = new List<PdbBaseMember>()
                {
                    new PdbTypeMember(symbol.Symbol)
                };
            }
        }
    }
}

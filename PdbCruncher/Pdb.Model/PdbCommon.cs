﻿using Dia2Lib;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;

namespace PdbModel
{
    [SuppressUnmanagedCodeSecurity]
    internal static class SafeNativeMethods
    {
        [DllImport("shlwapi.dll", CharSet = CharSet.Unicode)]
        public static extern int StrCmpLogicalW(string psz1, string psz2);
    }

    internal sealed class NaturalStringComparer : IComparer<string>
    {
        public int Compare(string a, string b)
        {
            return SafeNativeMethods.StrCmpLogicalW(a, b);
        }
    }

    internal sealed class NaturalFileInfoNameComparer : IComparer<FileInfo>
    {
        public int Compare(FileInfo a, FileInfo b)
        {
            return SafeNativeMethods.StrCmpLogicalW(a.Name, b.Name);
        }
    }

    internal enum CV_access_e
    {
        CV_private = 1,
        CV_protected = 2,
        CV_public = 3
    }

    internal enum LocationType
    {
        LocIsNull,
        LocIsStatic,
        LocIsTLS,
        LocIsRegRel,
        LocIsThisRel,
        LocIsEnregistered,
        LocIsBitField,
        LocIsSlot,
        LocIsIlRel,
        LocInMetaData,
        LocIsConstant,
        LocIsRegRelAliasIndir,
        LocTypeMax
    }

    internal enum DataKind
    {
        DataIsUnknown,
        DataIsLocal,
        DataIsStaticLocal,
        DataIsParam,
        DataIsObjectPtr,
        DataIsFileStatic,
        DataIsGlobal,
        DataIsMember,
        DataIsStaticMember,
        DataIsConstant
    }

    internal enum UdtKind
    {
        UdtStruct,
        UdtClass,
        UdtUnion,
        UdtInterface
    }

    internal enum CV_modifier_e
    {
        // 0x0000 - 0x01ff - Reserved.

        CV_MOD_INVALID = 0x0000,

        // Standard modifiers.

        CV_MOD_CONST = 0x0001,
        CV_MOD_VOLATILE = 0x0002,
        CV_MOD_UNALIGNED = 0x0003,

        // 0x0200 - 0x03ff - HLSL modifiers.

        CV_MOD_HLSL_UNIFORM = 0x0200,
        CV_MOD_HLSL_LINE = 0x0201,
        CV_MOD_HLSL_TRIANGLE = 0x0202,
        CV_MOD_HLSL_LINEADJ = 0x0203,
        CV_MOD_HLSL_TRIANGLEADJ = 0x0204,
        CV_MOD_HLSL_LINEAR = 0x0205,
        CV_MOD_HLSL_CENTROID = 0x0206,
        CV_MOD_HLSL_CONSTINTERP = 0x0207,
        CV_MOD_HLSL_NOPERSPECTIVE = 0x0208,
        CV_MOD_HLSL_SAMPLE = 0x0209,
        CV_MOD_HLSL_CENTER = 0x020a,
        CV_MOD_HLSL_SNORM = 0x020b,
        CV_MOD_HLSL_UNORM = 0x020c,
        CV_MOD_HLSL_PRECISE = 0x020d,
        CV_MOD_HLSL_UAV_GLOBALLY_COHERENT = 0x020e,

        // 0x0400 - 0xffff - Unused.

    }

    internal enum BasicType
    {
        btNoType = 0,
        btVoid = 1,
        btChar = 2,
        btWChar = 3,
        btSChar = 4,
        btUChar = 5,
        btInt = 6,
        btUInt = 7,
        btFloat = 8,
        btBCD = 9,
        btBool = 10,
        btShort = 11,
        btUShort = 12,
        btLong = 13,
        btULong = 14,
        btInt8 = 15,
        btInt16 = 16,
        btInt32 = 17,
        btInt64 = 18,
        btInt128 = 19,
        btUInt8 = 20,
        btUInt16 = 21,
        btUInt32 = 22,
        btUInt64 = 23,
        btUInt128 = 24,
        btCurrency = 25,
        btDate = 26,
        btVariant = 27,
        btComplex = 28,
        btBit = 29,
        btBSTR = 30,
        btHresult = 31,
        btChar16 = 32,
        btChar32 = 33,
        btChar8 = 34,
    }

    internal static class BasicTypeHelpers
    {
        public static string GetName(IDiaSymbol type)
        {
            var baseType = (BasicType)type.baseType;

            switch (baseType)
            {
                case BasicType.btNoType:
                    return "<no-type>";
                case BasicType.btVoid:
                    return "void";
                case BasicType.btChar:
                    return "char";
                case BasicType.btWChar:
                    return "wchar_t";
                case BasicType.btSChar:
                    return "signed char";
                case BasicType.btUChar:
                    return "unsigned char";
                case BasicType.btInt:
                case BasicType.btUInt:
                    {
                        var result = baseType == BasicType.btUInt ? "unsigned " : string.Empty;

                        switch (type.length)
                        {
                            case 1:
                                result += "char";
                                break;
                            case 2:
                                result += "short";
                                break;
                            case 4:
                                result += "int";
                                break;
                            case 8:
                                result += "long long";
                                break;
                        }

                        return result;
                    }
                case BasicType.btFloat:
                    {
                        if (type.length == 4)
                        {
                            return "float";
                        }
                        else if (type.length == 8)
                        {
                            return "double";
                        }
                        else if (type.length == 2)
                        {
                            return "half";
                        }

                        return "<unknown-float>";
                    }
                case BasicType.btBCD:
                    return "<bcd>";
                case BasicType.btBool:
                    return "bool";
                case BasicType.btShort:
                    return "short";
                case BasicType.btUShort:
                    return "unsigned short";
                case BasicType.btLong:
                    return "long";
                case BasicType.btULong:
                    return "unsigned long";
                case BasicType.btInt8:
                    return "__int8";
                case BasicType.btInt16:
                    return "__int16";
                case BasicType.btInt32:
                    return "__int32";
                case BasicType.btInt64:
                    return "__int64";
                case BasicType.btInt128:
                    return "__int128";
                case BasicType.btUInt8:
                    return "__uint8";
                case BasicType.btUInt16:
                    return "__uint16";
                case BasicType.btUInt32:
                    return "__uint32";
                case BasicType.btUInt64:
                    return "__uint64";
                case BasicType.btUInt128:
                    return "__uint128";
                case BasicType.btCurrency:
                    return "<currency>";
                case BasicType.btDate:
                    return "<date>";
                case BasicType.btVariant:
                    return "<variant>";
                case BasicType.btComplex:
                    return "<complex>";
                case BasicType.btBit:
                    return "<bit>";
                case BasicType.btBSTR:
                    return "BSTR";
                case BasicType.btHresult:
                    return "HRESULT";
                case BasicType.btChar16:
                    return "char16_t";
                case BasicType.btChar32:
                    return "char32_t";
                case BasicType.btChar8:
                    return "char8_t";
            }

            throw new ArgumentOutOfRangeException(nameof(type));
        }
    }
    internal enum NameSearchOptions
    {
        nsNone,
        nsfCaseSensitive = 0x1,
        nsfCaseInsensitive = 0x2,
        nsfFNameExt = 0x4,
        nsfRegularExpression = 0x8,
        nsfUndecoratedName = 0x10,
    }
}

﻿using Dia2Lib;
using System;
using System.Collections.Generic;
using System.Text;

namespace PdbModel
{
    public class PdbType
    {
        internal IDiaSymbol Symbol { get; private set; }
        public string TypeName { get; private set; }

        public PdbType(IDiaSymbol symbol)
        {
            this.Symbol = symbol;
            this.TypeName = symbol.name;
        }
    }
}

﻿using Dia2Lib;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PdbModel
{
    public sealed class PdbFile
    {
        private IDiaDataSource m_Source;
        private IDiaSession m_Session;
        private List<PdbType> m_Types;

        public IReadOnlyList<PdbType> Types
        {
            get
            {
                return m_Types;
            }
        }

        public PdbFile(string path)
        {
            try
            {
                this.m_Source = new DiaSourceAltClass();
                this.m_Source.loadDataFromPdb(path);
                this.m_Source.openSession(out this.m_Session);

                this.m_Session.globalScope.findChildren(
                    SymTagEnum.SymTagUDT,
                    null,
                    (uint)NameSearchOptions.nsNone,
                    out IDiaEnumSymbols enumSymbols
                    );

                var types = new Dictionary<string, PdbType>();

                foreach (IDiaSymbol symbol in enumSymbols)
                {
                    if (symbol != null && symbol.length > 0)
                    {
                        if (!types.ContainsKey(symbol.name))
                        {
                            types.Add(symbol.name, new PdbType(symbol));
                        }
                    }
                }

                m_Types = new List<PdbType>(
                    types.Values.OrderBy(
                        x => x.TypeName,
                        new NaturalStringComparer()
                        )
                    );
            }
            catch(Exception e)
            {
                throw new PdbException("Failed to create PDB file", e);
            }
        }
    }
}
